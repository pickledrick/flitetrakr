package input

import (
	"strings"
)

func FilterConnections(line string) []string {
	var connections []string

	if strings.HasPrefix(line, "Connection:") {
		connections = strings.Split(line, ":")
		connections = strings.Split(connections[1], ",")
		for i, v := range connections {
			connections[i] = strings.TrimSpace(v)
		}
		return connections
	}

	return connections
}

func FilterCost(line string) string {
	s := strings.SplitAfter(line, "connection")
	out := strings.Replace(s[1], "?", "", -1)
	out = strings.TrimSpace(out)
	return out
}

func FilterCheapest(line string) string {
	s := strings.SplitAfter(line, "from")
	out := strings.Replace(s[1], "?", "", -1)
	out = strings.TrimSpace(out)
	new := strings.Split(out, " ")

	new = append(new[:1], new[1+1:]...)
	out = strings.Join(new, "-")
	return out
}
