package handler

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/thebeefcake/flitetrakr/app/input"
	graph "github.com/gyuho/goraph"
)

func Handle(line string, g graph.Graph) {

	var selector string

	if strings.HasPrefix(line, "What is the price of the connection") {
		selector = "cost"
	} else if strings.HasPrefix(line, "What is the cheapest connection from") {
		selector = "gunstig"
	} else if strings.HasPrefix(line, "Connection:") {
		selector = "blank"
	}

	switch selector {
	case "cost":
		path := input.FilterCost(line)
		getWeight(g, path)
	case "gunstig":
		path := input.FilterCheapest(line)
		getCheapest(g, path)
	case "blank":
	default:
		fmt.Println("Please input a valid command.")
	}
}

func CreateConnections(line string) graph.Graph {
	g := CreateGraph(input.FilterConnections(line))
	return g
}

func CreateGraph(data []string) graph.Graph {
	g := graph.NewGraph()

	for _, v := range data {
		var hop []string
		hop = strings.Split(v, "-")
		nd1, err := g.GetNode(graph.StringID(hop[0]))
		if err != nil {
			nd1 = graph.NewNode(hop[0])
			g.AddNode(nd1)
		}
		nd2, err := g.GetNode(graph.StringID(hop[1]))
		if err != nil {
			nd2 = graph.NewNode(hop[1])
			g.AddNode(nd2)
		}
		weight, _ := strconv.ParseFloat(hop[2], 64)
		g.ReplaceEdge(nd1.ID(), nd2.ID(), weight)
	}
	return g
}

func getWeight(g graph.Graph, path string) {

	routeInput := strings.Split(path, "-")
	var hops []string
	last := routeInput[0]

	strings.Split(path, "-")

	for i := 0; i < len(routeInput)-1; i++ {
		hop := last + "-" + routeInput[i+1]
		hops = append(hops, hop)
		last = routeInput[i+1]
	}
	printCost := true
	var cost int
	for _, v := range hops {
		hop := strings.Split(v, "-")
		departure := hop[0]
		arrival := hop[1]

		weight, err := g.GetWeight(graph.StringID(departure), graph.StringID(arrival))
		if err != nil {
			fmt.Println("No such connection found!")
			printCost = false
		}
		cost = cost + int(weight)
	}

	if printCost {
		fmt.Println(cost)
	}

}

func getCheapest(g graph.Graph, path string) {

	hops := strings.Split(path, "-")

	p, _, err := graph.Dijkstra(g, graph.StringID(hops[0]), graph.StringID(hops[1]))
	var bestPath []string
	for _, v := range p {
		bestPath = append(bestPath, v.String())
	}
	fmt.Println(strings.Join(bestPath, "-"))

	if err != nil {
		panic(err)
	}

}

func testPrint(g graph.Graph) {
	for _, v := range graph.DFS(g, graph.StringID("NUE")) {
		fmt.Println(v.String())
	}

}

func getAllRoutes(g graph.Graph, source string, destination string, visited map[string]bool, path []string) {
	count := 1
	visited[source] = true
	path = append(path, source)

	if source == destination || count > 1 {
		fmt.Println(path)
		count = count + 1
	} else {
		//fmt.Println("stub")
		for _, v := range graph.DFS(g, graph.StringID(source)) {
			if visited[v.String()] == false {
				printAllPaths(v.String(), destination, visited, g)
			}
		}

	}
	if len(path) > 0 {
		path = path[:len(path)-1]
	}
	visited[source] = false
}

func printAllPaths(source string, destination string, visited map[string]bool, g graph.Graph) {
	var path []string
	for key, _ := range visited {
		visited[key] = false
	}
	getAllRoutes(g, source, destination, visited, path)
}
