package main

import (
	"bufio"
	"os"
	"strings"

	"bitbucket.org/thebeefcake/flitetrakr/app/handler"
	graph "github.com/gyuho/goraph"
)

func main() {

	scanner := bufio.NewScanner(os.Stdin)
	var g graph.Graph

	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "Connection") {
			g = handler.CreateConnections(scanner.Text())
		}
		handler.Handle(scanner.Text(), g)
	}
}
