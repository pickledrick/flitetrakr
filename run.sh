#!/usr/bin/env sh

echo "##############################################"
echo "### Building binary and creating container ###"
echo "##############################################"
GOOS=linux GOARCH=amd64 go build -o flitetrakr ./app
docker build . -t flitetrakr:app
echo "#############################################"
echo "### Running input file against container  ###"
echo "#############################################"
cat input.txt | docker run -i --rm flitetrakr:app
