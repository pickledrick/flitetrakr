## flitetrakr

### requirements
- docker
- bash

### to run

ensure input.txt contains input data

    ./run.sh

### to do
- price on cheapest route
- multiple paths
- logic for hops/cost for multiple paths
