FROM alpine

ADD flitetrakr /go/bin/

CMD cd /go/bin/ && ./flitetrakr
